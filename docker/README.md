Dockerfile for audinate/zephyr:zsdk-0.13.0

Build command

docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t audinate/zephyr:zsdk-0.13.0 .

