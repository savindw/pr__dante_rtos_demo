/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <sys/printk.h>

int testFunc(int testVar)
{
	while( testVar < 100 )
	{
		testVar += 10;
		printk("%s: test variable = %d\n", __func__, testVar);
	}
	return testVar;
}

void main(void)
{
	int testVar = 0;
	printk("Hello World! %s\n", CONFIG_BOARD);
	printk("%s: test variable = %d\n", __func__, testVar);

	testVar = testFunc(testVar);
	printk("%s: test variable = %d\n", __func__, testVar);
}
